const roleCommands = require("./roleCommands");

const Discord = require("discord.js");
const client = new Discord.Client();

const server = new Map();

const add = "addRole";
const remove = "rmvRole";
const roles = "roles";
const hlp = "help";

//https://discordapp.com/oauth2/authorize?client_id=554561033243721738&scope=bot&permissions=268435456

client.on("ready", () => {
    console.log(`Logged in as ${client.user.tag}!`);
    client.user.setActivity("strawberry help");
});

function counterMessage(msg) {
    const serverName = msg.guild.name;
    const channel = msg.channel;
    const userName = msg.author.username;
    const userId = msg.author.discriminator;

    if (`${userName}#${userId}` === "Elio#4307") {
        let nbMessages = server.get("${userName}#${userId}")+1;
        if (nbMessages >= 50) {
            channel.send("Miaou !!!");
            server.set("${userName}#${userId}", 0);
        }
    }

    // const key = `${serverName}|${userName}#${userId}`;
    // let nbMessages = server.get(key);

    // if (!nbMessages) nbMessages = 0;

    // server.set(key, ++nbMessages);

    // if (nbMessages == 5) {
    //     channel.send("Vas dans le métros satanas");
    //     server.set(key, 0);
    // }
}

//todo : addRole, removeRole, roles, help, admin command
client.on("error", console.error);

client.on("message", msg => {
    const content = msg.content;
    const roles = msg.guild.roles;
    const channel = msg.channel;
    const botRole = roles.find(role => role.name === "strawberry");
    //console.log(roles.find, botRole);

    if (content.includes(" ")) {
        let parts = content.split(" ");
        if (parts[0] == "strawberry") {
            switch (parts[1]) {
                case roleCommands.roles.title:
                    roleCommands.roles.action(roles, botRole, channel);
                    break;
                case roleCommands.hlp.title:
                    roleCommands.hlp.action();

                    let fields = [];
                    let commandsCopy = Object.assign({}, roleCommands);
                    delete commandsCopy.hlp;

                    Object.keys(commandsCopy).forEach((key, index) => {
                        let command = commandsCopy[key];
                        fields.push({
                            name: command.title,
                            value: command.description,
                            inline: true
                        });
                    });

                    channel.send({
                        embed: {
                            title: "**Help**",
                            description: "Voici la liste des commandes :",
                            color: 1254,
                            fields: fields
                        }
                    });
                    break;
                case roleCommands.add.title:
                    //console.log("add");
                    roleCommands.add.action(msg, roles, parts[2]);
                    break;
                case roleCommands.rmv.title:
                    roleCommands.rmv.action(msg, roles, parts[2]);
                    break;
            }
        } else {
            counterMessage(msg);
        }
    } else {
        counterMessage(msg);
    }

    if (msg.content === "ping") {
        msg.reply("Pong!");
    }
});

client.login("NTU0NTYxMDMzMjQzNzIxNzM4.D2ecMw.PVg6OsHwyhRb2sIxO4HJzmTmZcU");
