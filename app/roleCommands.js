const roleCommands = {
    add: {
        title: "addRole",
        description: (() => {
            return "Vous ajoute un role.";
        })(),
        action: (msg, roles, arg) => {
            let requestedRole = null;
            roles.forEach(role => {
                if (role.name == arg) {
                    requestedRole = role;
                }
            });

            if (requestedRole !== null) {
                let user = msg.member;
                user.addRole(requestedRole.id)
                    .then(() => {
                        msg.reply("Congratulations you have a new role");
                    })
                    .catch(err => {
                        msg.reply(
                            `Argh! I can't for this reason ${JSON.stringify(
                                err
                            )}`
                        );
                    });
            } else {
                msg.reply(
                    "Hey I can't do that because it's not an existing role"
                );
            }
        }
    },
    rmv: {
        title: "rmvRole",
        description: (() => {
            return "Vous retire un role.";
        })(),
        action: (msg, roles, arg) => {
            let toRemoveRole = null;
            roles.forEach(role => {
                if (role.name == arg) {
                    toRemoveRole = role;
                }
            });

            if (toRemoveRole !== null) {
                let user = msg.member;
                user.removeRole(toRemoveRole.id)
                    .then(() => {
                        msg.reply("Congratulations you have lost a role");
                    })
                    .catch(err => {
                        msg.reply(
                            `Argh! I can't for this reason ${JSON.stringify(
                                err
                            )}`
                        );
                    });
            } else {
                msg.reply(
                    "Hey I can't do that because it's not an existing role"
                );
            }
        }
    },
    hlp: {
        title: "help",
        description: (() => {
            return "Afficher l'aide.";
        })(),
        action: () => {}
    },
    roles: {
        title: "roles",
        description: "Liste les roles accessiblent.",
        action: (roles, botRole, channel) => {
            let desc = "";
            roles.forEach(role => {
                if (
                    botRole.comparePositionTo(role) > 0 &&
                    role.name != "@everyone"
                ) {
                    desc += role.name + "\n";
                }
            });

            channel.send({
                embed: {
                    title: "**Roles :**",
                    description: `Voici la liste des roles accessible:\n${desc}`,
                    color: 1254
                }
            });
        }
    }
};

module.exports = roleCommands;
